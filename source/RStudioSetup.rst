RStudio Setup
=============

Downloading R
-------------

The first step is to download the R software. You can download it for free from CRAN via the `R website <https://www.r-project.org/>`_. 

If you're confused about which version you need, you can download the Windows (32-bit/64-bit) version `here <https://cloud.r-project.org/bin/windows/base/R-3.5.2-win.exe>`_ (79 MB).

Installing R
------------

Once the download is complete, navigate to your ``Downloads`` folder, and run the executable named ``R-3.5.2-win.exe`` (or similar).

This will open the R installer. You can accept all default settings. First, press OK:

.. image:: ./images/R_1.PNG
    :width: 200pt
    :align: center 

Then Next:

.. image:: ./images/R_2.PNG
    :width: 300pt
    :align: center 

Then Next again:

.. image:: ./images/R_3.PNG
    :width: 300pt
    :align: center 

Then Next again:

.. image:: ./images/R_4.PNG
    :width: 300pt
    :align: center 

Then Next again:

.. image:: ./images/R_5.PNG
    :width: 300pt
    :align: center 

Then Next again:

.. image:: ./images/R_6.PNG
    :width: 300pt
    :align: center 

Then Next once more:

.. image:: ./images/R_7.PNG
    :width: 300pt
    :align: center 


Wait until the installation has completed:

.. image:: ./images/R_8.PNG
    :width: 300pt
    :align: center 

Then click Finish to complete:

.. image:: ./images/R_9.PNG
    :width: 300pt
    :align: center 



What is RStudio?
----------------

RStudio is an ‘integrated development environment’ (IDE) used with the R programming language. IDEs are pieces of software that include features for advanced editing, interactive testing, and debugging. There exist many IDEs, and you may find in time that you find an IDE that suits you, or alternatively, you may find that you prefer to write scripts in a simple text editor and prefer not to use an IDE at all.

RStudio is a very widely used IDE, and we can recommend it as a place to start.

Downloading RStudio
-------------------

You can download RStudio for free from the `RStudio website <https://www.rstudio.com/products/rstudio/download/>`_. Make sure you choose the correct version, which is **RStudio Desktop installer (free)** for Windows Vista/7/8/10.

If you’re struggling to find the correct version, you can also download it directly `here <https://download1.rstudio.org/RStudio-1.1.463.exe>`_ (85.8 MB). 

.. NOTE::
    Are you using Linux or Mac, or an earlier version of Windows? The following instructions should mostly work fine for you, but be aware you'll need to select a different version from the RStudio download site, and that RStudio will look visually slightly different.

Installing RStudio
------------------

Once the download is complete, navigate to your ``Downloads`` folder, and run the executable named ``RStudio-1.1.463.exe`` (or similar).

This will open the RStudio installer. You can accept all default settings. Press next:

.. image:: ./images/rstudio_1.PNG
    :width: 300pt
    :align: center 

then next again:

.. image:: ./images/rstudio_2.PNG
    :width: 300pt
    :align: center 

then next once more:

.. image:: ./images/rstudio_3.PNG
    :width: 300pt
    :align: center 

...and wait until the installation is complete:

.. image:: ./images/rstudio_4.PNG
    :width: 300pt
    :align: center 

Press Finish to complete the installation.

.. image:: ./images/rstudio_5.PNG
    :width: 300pt
    :align: center 

Running RStudio
---------------

You'll find a new program named ``RStudio`` in your Start menu. Click the icon to open RStudio. You should be presented with a program that looks like the following:

.. image:: ./images/rstudio_9.PNG
    :width: 500pt
    :align: center 


Installation problems?
----------------------

If something isn't working for you, please feel free to get in touch with sam.bowers@ed.ac.uk.
