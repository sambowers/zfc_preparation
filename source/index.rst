.. ZFC Preparation documentation master file, created by
   sphinx-quickstart on Thu Feb  7 14:37:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ZFC preparation materials
=========================

Welcome to the preparation materials for the workshop for the study of drivers of deforestation, forest degradation and national forest definition. The workshop will take place in Harare on 25th February - 1st March 2019. To prepare for the workshop, we've prepared some material for you to work through.

For part of the workshop we’ll be covering the basics of R, a programming language commonly used for data analysis. We’ll be running it through the ‘RStudio’ development environment, and using it for the analysis of forest inventory data from Zimbabwe. The purpose of this document is to prepare all participants by installing required software and familiarisation with RStudio’s user interface. We'll also run through the installation process of QGIS.

We’d like all participants to have at least tried to run through these instructions before the workshop begins on Monday 25th February.

Objectives
==========

#. To install RStudio and QGIS in preparation for the workshop
#. To familiarise each participant with the layout and operation of RStudio
#. To produce some simple summary statistics and graphs using forest inventory data

Contents
========

.. toctree::
   :maxdepth: 1
   :numbered:
   
   RStudioSetup.rst
   RStudioIntro.rst
   RStudioData.rst
   QGISSetup.rst

   
Requirements
============

#. A Windows PC. Preferably a laptop that can be brought along to the workshop.
#. An internet connection suitable for downloading moderately large files (~100s MB).
#. A sense of adventure!

Contact
=======

If you encounter problems or need any assitance, please feel free to email Sam Bowers at sam.bowers@ed.ac.uk.